'use strict'

const SVG = require('svg-captcha');
const AWS = require('aws-sdk');
const UUID = require('uuid/v4');

exports.handler = async(event) => {
  const db = new AWS.DynamoDB({ apiVersion: "2012-08-10" })
  const documentClient = new AWS.DynamoDB.DocumentClient()

  try {
    const captcha = await SVG.create({
      noise: 2
    });
    const uid = UUID();
    const now = new Date();

    // Set expiry
    now.setMinutes(now.getMinutes() + 10); // 10 minutes from creation time

    // Write session to DynamoDB
    const dbparams = {
      TableName: process.env.DYNAMODB_TABLE,
      Item: {
        id: uid,
        text: captcha.text,
        ttl: Math.floor(now.getTime() / 1000)
      }
    }

    const data = await documentClient.put(dbparams).promise();

    // Return response to request
    const response = {
      statusCode: 200,
      headers: { 
        'Content-Type': 'application/json',
        "Access-Control-Allow-Origin": "*"
      },
      body: JSON.stringify({
        id: uid,
        captcha: captcha.data
      })
    }

    return response

  }
  catch (error) {
    return {
      statusCode: error.statusCode || 501,
      headers: { 'Content-Type': 'text/plain' },
      body: 'Couldn\'t create session.',
    }
  }
}
