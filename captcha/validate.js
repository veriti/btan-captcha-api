'use strict'

const AWS = require('aws-sdk');

exports.handler = async(event) => {
  const documentClient = new AWS.DynamoDB.DocumentClient();

  const response = {
    statusCode: 404,
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      error: "Invalid request"
    })
  };

  try {
    // Check if request body is present
    if (!event.body || !Object.keys(event.body).length) return response;

    const body = JSON.parse(event.body);

    // Check if required parameters are met
    if (!body.id || typeof body.id !== "string") return response;
    if (!body.text || typeof body.id !== "string") return response;

    // Get session from DynamoDB
    const dbparams = {
      TableName: process.env.DYNAMODB_TABLE,
      Key: {
        id: body.id
      }
    };

    const data = await documentClient.get(dbparams).promise();

    // Check if session is found
    if (!data || !Object.keys(data).length) {
      return {
        ...response,
        body: JSON.stringify({
          error: "Session not found"
        })
      };
    }

    // Check if session is expired
    const now = new Date();
    const expire = new Date(data.Item.ttl * 1000);

    if (now > expire) {
      return {
        ...response,
        statusCode: 401,
        body: JSON.stringify({
          error: "Session expired"
        })
      };
    }

    // Check if text matches
    if (data.Item.text !== body.text) {
      return {
        ...response,
        statusCode: 401,
        body: JSON.stringify({
          error: "Text mismatch"
        })
      };
    }

    const newDBParams = {
      TableName: process.env.DYNAMODB_TABLE,
      Key: {
        id: body.id
      }
    }

    const newData = await documentClient.delete(dbparams).promise();

    return {
      ...response,
      statusCode: 200,
      body: JSON.stringify({
        session: body.id
      })
    };
  }
  catch (error) {
    return {
      ...response,
      statusCode: error.statusCode || 501,
      body: JSON.stringify(error)
    };
  }
};
