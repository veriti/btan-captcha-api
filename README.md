# Deploying to AWS

## Requirements

- NodeJS 10.16.0 (or later)

## Installation

#### 1. Setup AWS Credentials

- Login to AWS console and search for **IAM (Manage User Access...)** on services
- From the sidebar menu, click **Users**
- On the **Users** page, create a new user by clicking the **Add user** button
- Set the **User name*** accordingly. Ex. **serverless**
- Make sure to check on **Programmatic access**
- Go to the next step by clicking **Next: Permissions** button
- **Attach existing policies directly** to the user and check **AdministratorAccess**
- Click on **Next:Review**
- Create user by clicking **Create user** button
- _IMPORTANT_: Note of the **Access key ID** and **Secret access key** (reveal it by clicking the "Unhide" linked text), this will be used to setup serverless framework


#### 2. Setup serverless CLI

- Install serverless framework CLI by running the following command
```bash
npm install -g serverless
# verify installation (optional)
serverless
```

- Configure credentials by running the command below, replacing ACCESS_KEY_ID and SECRET_ACCESS_KEY accordingly
```bash
serverless config credentials --provider aws --key ACCESS_KEY_ID --secret SECRET_ACCESS_KEY
```

## Deployment

- Navigate to the project path on your terminal and run

```
# install dependencies
npm install

# run deployment
serverless deploy
```

## API Endpoints

Please note of the API endpoints (URL) to post to developers

